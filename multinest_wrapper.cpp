//
// Created by Mehmet Deniz Aksulu on 2019-05-16.
//

#include "multinest_wrapper.h"

int world_rank, world_size;

multinest_wrapper::multinest_wrapper(std::vector<double> _min_params, std::vector<double> _max_params) {
#ifdef OPT_MPI_MN
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
#endif
  min_params = _min_params;
  max_params = _max_params;
  n_dim = min_params.size();
  n_free_dim = min_params.size();
  for (int i = 0; i < min_params.size(); i++) {
    if (min_params[i] == max_params[i]) {
      n_free_dim--;
    } else {
      free_parameter_index.push_back(i);
    }
  }
}

multinest_results multinest_wrapper::fit(double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                                         void *_args) {
  func_fitness = _func_fitness;
  args = _args;

  multinest_results results;
  int ndims = n_free_dim;                    // dimensionality (no. of free parameters)
  int nPar = n_free_dim;                    // total no. of parameters including free & derived parameters
  int pWrap[ndims];                            // which parameters to have periodic boundary conditions?
  for (int i = 0; i < ndims; i++) pWrap[i] = 0;
  int seed = -1;                            // random no. generator seed, if < 0 then take the seed from system clock
  nested::run(properties.IS,
              properties.mmodal,
              properties.ceff,
              properties.nlive,
              properties.tol,
              properties.efr,
              ndims,
              nPar,
              properties.nClsPar,
              properties.maxModes,
              properties.updInt,
              properties.Ztol,
              properties.output_dir,
              seed,
              pWrap,
              properties.fb,
              properties.resume,
              properties.outfile,
              properties.initMPI,
              properties.logZero,
              properties.maxiter,
              LogLike,
              dumper,
              this);
#ifdef OPT_MPI_MN
  double bestfit_likelihoods_local[world_size];
  double bestfit_likelihoods[world_size];
  for (int i = 0; i < world_rank; i++) bestfit_likelihoods_local[i] = 0;
  bestfit_likelihoods_local[world_rank] = bestfit_likelihood;
  MPI_Allreduce(bestfit_likelihoods_local, bestfit_likelihoods, world_size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  int bestfit_likelyhood_index = 0;
  bestfit_likelihood = bestfit_likelihoods[0];
  for (int i = 0; i < world_size; i ++){
    if (bestfit_likelihood > bestfit_likelihoods[i]) {
      bestfit_likelihood = bestfit_likelihoods[i];
      bestfit_likelyhood_index = i;
    }
  }
  MPI_Bcast(bestfit_param.data(), bestfit_param.size(), MPI_DOUBLE, bestfit_likelyhood_index, MPI_COMM_WORLD);
  MPI_Bcast(&bestfit_likelihood, 1, MPI_DOUBLE, bestfit_likelyhood_index, MPI_COMM_WORLD);
#endif
  results.bestfit_param = bestfit_param;
  results.bestfit_likelihood = bestfit_likelihood;
  return results;
}

multinest_wrapper::~multinest_wrapper() {

}

const multinest_properties &multinest_wrapper::GetProperties() const {
  return properties;
}

void multinest_wrapper::SetProperties(const multinest_properties &properties) {
  multinest_wrapper::properties = properties;
}

void multinest_wrapper::safe_print(FILE *pFile, const char *message, ...) {
  if (properties.silent == 0) {
#ifdef OPT_MPI_MN
    if (world_rank == 0) {
#endif

      va_list _va_args;
      va_start(_va_args, message);
      vfprintf(pFile, message, _va_args);
      va_end(_va_args);

#ifdef OPT_MPI_MN
    }
#endif
  }
}

void LogLike(double *Cube, int &ndim, int &npars, double &lnew, void *context) {
  multinest_wrapper *my_class = (multinest_wrapper *) context;
  std::vector<double> parameters(my_class->n_dim);
  int k = 0;
  for (int i = 0; i < my_class->n_dim; i++) {
    if (my_class->max_params[i] != my_class->min_params[i]) {
      double temp_param =
          my_class->min_params[i] + (my_class->max_params[i] - my_class->min_params[i]) * Cube[k++];
      parameters[i] = temp_param;
    } else {
      parameters[i] = my_class->max_params[i];
    }
  }
  lnew = my_class->func_fitness(parameters, my_class->args);
  if (lnew > my_class->bestfit_likelihood) {
    my_class->bestfit_likelihood = lnew;
    my_class->bestfit_param = parameters;
  }

}

void dumper(int &nSamples,
            int &nlive,
            int &nPar,
            double **physLive,
            double **posterior,
            double **paramConstr,
            double &maxLogLike,
            double &logZ,
            double &INSlogZ,
            double &logZerr,
            void *context) {
}