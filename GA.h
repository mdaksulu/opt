//
// Created by Mehmet Deniz Aksulu on 2019-04-02.
//

#ifndef OPTIMIZATION_GA_H
#define OPTIMIZATION_GA_H

#include <algorithm>
#include <stdarg.h>
#include <cmath>
#include <vector>
#include <string>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "simplex.h"
#include "environment.h"

#ifdef OPT_MPI_GA
#include <mpi.h>
#endif

#define CHAR_BUFFER 255
#define SMALL_DOUBLE -1e90

typedef enum { ga_sel_tournament, ga_sel_probability } GA_selection_scheme;
typedef enum { ga_cross_blx_alpha, ga_cross_normal_alpha, ga_cross_rayleigh_alpha } GA_crossover_scheme;

typedef struct {
  int max_iteration = 5000;
  int population_size = 10;
  double alpha = 0.5;
  double mutation_prob = 0.1;
  double mutation_conv = 2.0;
  double ftol = 1e-7;
  GA_selection_scheme selection_scheme = ga_sel_tournament;
  GA_crossover_scheme crossover_scheme = ga_cross_blx_alpha;
  int silent = 0;
  int simplex_threshold = 100;
  simplex_properties simp_prop;
} GA_properties;

typedef struct {
  std::vector<double> fitness;
  std::vector<std::vector<double> > population;
  int elite_index;
  int iter_no;
  int eval_no;
} GA_result;

class GA {
 public:
  GA(std::vector<double> _min_params, std::vector<double> _max_params);
  virtual ~GA();
  GA_result fit(double (*_func_fitness)(std::vector<double> _parameters, void *_args), void *_args);

  const std::vector<double> &getFitness() const;
  const std::vector<std::vector<double> > &getPopulation() const;
  const GA_properties &getGaProperties() const;
  void setGaProperties(const GA_properties &gaProperties);
  const std::vector<std::string> &getParameterNames() const;
  void setParameterNames(const std::vector<std::string> &parameterNames);
 private:
  std::vector<double> fitness;
  std::vector<std::vector<double> > population;
  std::vector<int> free_parameter_index;

  int elite_index;
  int worse_index;
  int old_elite_index;
  int no_change_iteration;
  int n_boundary_crossing;

  std::vector<double> elite_individual;
  double elite_fitness;

  GA_properties ga_properties;

  std::vector<double> min_params;
  std::vector<double> max_params;

  int n_dim;
  int n_free_dim;
  int iter_no;
  int eval_no;

  double (*func_fitness)(std::vector<double> _parameters, void *_args);
  void *args;

  std::vector<std::vector<double> > generate_random_population();
  std::vector<std::vector<double> > BLX_alpha_crossover();
  std::vector<std::vector<double> > normal_alpha_crossover();
  std::vector<std::vector<double> > rayleigh_alpha_crossover();
  std::vector<std::vector<double> > non_uniform_mutation();
  void select_parents_tournament(int *parent_1, int *parent_2);
  void select_parents_probability(int *parent_1, int *parent_2);
  void calculate_fitness();

  std::vector<std::string> parameter_names;

  const gsl_rng_type *T;
  gsl_rng *r;

  void print_population(std::vector<double> _fitness);
  double find_min(std::vector<double> _fitness);
  double find_max(std::vector<double> _fitness);

  void update_probability();
  std::vector<double> selection_probability;
  std::vector<double> cumulative_selection_probability;
  void safe_print(const char *message, ...);

  simplex* simp;
};

#endif //OPTIMIZATION_GA_H
