//
// Created by Mehmet Deniz Aksulu on 2019-05-16.
//

#ifndef BOXFIT_OPT_MULTINEST_WRAPPER_H_
#define BOXFIT_OPT_MULTINEST_WRAPPER_H_

#include <stdarg.h>
#include <algorithm>
#include <cmath>
#include <vector>
#include <string>
#include <limits>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_qrng.h>
#include <gsl/gsl_randist.h>
#include <numeric>
#include <random>
#include <stdlib.h>
#include "multinest.h"
#include "environment.h"

#define OPT_MPI_MN
#ifdef OPT_MPI_MN
#include <mpi.h>
#endif



typedef struct {
  int IS = 1;                     // do nested importance sampling.
  int mmodal = 0;                 // do mode separation
  int nClsPar = 0;
  int ceff = 0;                   // run in constance efficiency mode
  int nlive = 100;                // number of live points
  double efr = 0.8;               // set the required efficiency
  double tol = 0.5;               // tol, defines the stopping criteria
  int updInt = 1;              // after how many iterations feedback is required & the output files should be updated
  // note: posterior files are updated & dumper routine is called after every updInt*10 iterations
  double Ztol = -1 * std::numeric_limits<double>::max();                // all the modes with logZ < Ztol are ignored
  int maxModes = 100;             // expected max no. of modes (used only for memory allocation)
  std::string output_dir = "./";
  int fb = 0;                     // need feedback on standard output?
  int resume = 0;                 // resume from a previous job?
  int outfile = 1;                // write output files?
  int initMPI = 0;                // initialize MPI routines?, relevant only if compiling with MPI
  // set it to F if you want your main program to handle MPI initialization
  double logZero = -1 * std::numeric_limits<double>::max();// points with loglike < logZero will be ignored by MultiNest
  int maxiter =
      15000;  // max no. of iterations, a non-positive value means infinity. MultiNest will terminate if either it
  // has done max no. of iterations or convergence criterion (defined through tol) has been satisfied
  int silent = 0;
} multinest_properties;

typedef struct {
  std::vector<double > bestfit_param;
  double bestfit_likelihood;
} multinest_results;

class multinest_wrapper {
 public:
  multinest_wrapper(std::vector<double> _min_params, std::vector<double> _max_params);
  virtual ~multinest_wrapper();
  const multinest_properties &GetProperties() const;
  void SetProperties(const multinest_properties &properties);

  multinest_results fit(double (*_func_fitness)(std::vector<double> _parameters, void *_args), void *_args);

  multinest_properties properties;

  std::vector<double> min_params;
  std::vector<double> max_params;
  int n_dim;
  int n_free_dim;
  std::vector<int> free_parameter_index;

  void *args;
  double (*func_fitness)(std::vector<double> _parameters, void *_args);

  std::vector<double > bestfit_param;
  double bestfit_likelihood = -1 * std::numeric_limits<double>::max();

  void safe_print(FILE *pFile, const char *message, ...);
};

void LogLike(double *Cube, int &ndim, int &npars, double &lnew, void *context);
void dumper(int &nSamples,
            int &nlive,
            int &nPar,
            double **physLive,
            double **posterior,
            double **paramConstr,
            double &maxLogLike,
            double &logZ,
            double &INSlogZ,
            double &logZerr,
            void *context);
#endif //BOXFIT_OPT_MULTINEST_WRAPPER_H_
