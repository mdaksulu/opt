//
// Created by Mehmet Deniz Aksulu on 2019-07-29.
//

#ifndef BOXFIT_OPT_ENVIRONMENT_H_
#define BOXFIT_OPT_ENVIRONMENT_H_

extern int world_rank;
extern int world_size;

#define OPT_MPI_MN
//#define OPT_MPI_SIMPLEX
//#define OPT_MPI_GA

#endif //BOXFIT_OPT_ENVIRONMENT_H_
