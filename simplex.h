//
// Created by Mehmet Deniz Aksulu on 2019-04-03.
//

#ifndef OPTIMIZATION_SIMPLEX_H
#define OPTIMIZATION_SIMPLEX_H

#include <stdarg.h>
#include <algorithm>
#include <cmath>
#include <vector>
#include <string>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "environment.h"

#ifdef OPT_MPI_SIMPLEX
#include <mpi.h>
#endif

typedef struct {
  int max_iteration = 1000;
  double x_tol = 1e-7;
  double f_tol = 1e-7;
  double step_size = 0.2;
  double alpha = 1;
  double beta = 0.5;
  double gamma = 2;
  double delta = 0.5;
  int silent = 0;
} simplex_properties;

typedef struct {
  int iteration;
  std::vector<double> parameters;
  double func_val;
} simplex_result;

class simplex {
 public:
  simplex(std::vector<double> _min_param, std::vector<double> _max_param);
  virtual ~simplex();
  simplex_result fit(std::vector<double> initial_parameters,
                     double func_val,
                     double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                     void *_args);
  simplex_result fit(std::vector<std::vector<double> > initial_simplex,
                     double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                     void *_args);
  simplex_result fit(std::vector<std::vector<double> > initial_simplex, std::vector<double> initial_res,
                              double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                              void *_args);
  const simplex_properties &getProperties() const;
  void setProperties(const simplex_properties &properties);

 private:
  std::vector<double> min_param;
  std::vector<double> max_param;
  simplex_properties properties;
  std::vector<int> free_parameter_index;

  int n_dim;
  int n_free_dim;
  int iteration;
  int n_boundary_crossing;
  std::vector<std::vector<double> > simplex_vec;
  std::vector<double> simplex_val;
  std::vector<double> centroid;

  std::vector<std::vector<double> > initialize_simplex(std::vector<double> initial_parameters);
  void determine_indices();
  void calculate_centroid();
  std::vector<double> reflect();
  std::vector<double> expand(std::vector<double> reflected);
  std::vector<double> contract(std::vector<double> vec);
  void shrink();
  void print_simplex();
  void safe_print(const char *message, ...);

  const gsl_rng_type *T;
  gsl_rng *r;

  double (*func_fitness)(std::vector<double> _parameters, void *_args);
  void *args;

  int highest_index;
  int lowest_index;
  int second_lowest_index;

  std::vector<std::string> parameter_names;
};

#endif //OPTIMIZATION_SIMPLEX_H
