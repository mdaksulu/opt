//
// Created by Mehmet Deniz Aksulu on 2019-04-02.
//

#include "GA.h"

#ifdef OPT_MPI_GA
int world_rank, world_size;
#endif

GA::GA(std::vector<double> _min_params, std::vector<double> _max_params) {
#ifdef OPT_MPI_GA
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
#endif
  // initialize gsl random number generator
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r, time(NULL));

  min_params = _min_params;
  max_params = _max_params;

  n_dim = min_params.size();
  n_free_dim = min_params.size();
  for (int i = 0; i < min_params.size(); i++) {
    char temp_chr[CHAR_BUFFER];
    sprintf(temp_chr, "Parameter %3d", i);
    std::string temp_str = temp_chr;
    parameter_names.push_back(temp_str);
    if (min_params[i] == max_params[i]) {
      n_free_dim--;
    } else free_parameter_index.push_back(i);
  }

  eval_no = 0;
  iter_no = 0;
  old_elite_index = -1;
  elite_index = -1;
  no_change_iteration = 0;
  n_boundary_crossing = 0;
  safe_print("Number of free parameters: %3d\n", n_free_dim);
}

GA_result GA::fit(double (*_func_fitness)(std::vector<double> _parameters, void *_args), void *_args) {
  GA_result result;
  func_fitness = _func_fitness;
  args = _args;
  population = generate_random_population();

#ifdef OPT_MPI_GA
  for (int i = 0; i < ga_properties.population_size; i++) {
    MPI_Bcast(population[i].data(), population[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
  }
#endif

  while (iter_no < ga_properties.max_iteration) {
    calculate_fitness();

    double fdiff = (fitness[elite_index] - fitness[worse_index]) * (fitness[elite_index] - fitness[worse_index]);
    if (fdiff < ga_properties.ftol) {
      safe_print("GA fit has converged!\n");
      print_population(fitness);

      result.elite_index = elite_index;
      result.fitness = fitness;
      result.population = population;
      result.eval_no = eval_no;
      result.iter_no = iter_no;
      return result;
    }

    if (no_change_iteration >= ga_properties.simplex_threshold) {
      safe_print("Refining population with Nelder-Mead method.\n");
      std::vector<std::vector<double> > initial_simplex(n_free_dim + 1);
      std::vector<double> initial_simplex_res(n_free_dim + 1);
      std::vector<int> selected_simp_index(n_free_dim + 1);

      initial_simplex[0] = population[elite_index];
      initial_simplex_res[0] = fitness[elite_index];
      selected_simp_index[0] = elite_index;
      for (int i = 1; i < n_free_dim + 1; i++) {
        int random_index;
        bool exists;
        do {
          exists = false;
          random_index = (int) gsl_ran_flat(r, 0, population.size());
          for (int j = 0; j < i; j++) {
            if (random_index == selected_simp_index[j]) {
              exists = true;
              break;
            }
          }
        } while (exists);
        initial_simplex[i] = population[random_index];
        initial_simplex_res[i] = fitness[random_index];
        selected_simp_index[i] = random_index;
      }
#ifdef OPT_MPI_GA
      for (int i = 0; i < n_free_dim + 1; i++) {
        MPI_Bcast(initial_simplex[i].data(), initial_simplex[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
      }
      MPI_Bcast(initial_simplex_res.data(), initial_simplex_res.size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
      MPI_Barrier(MPI_COMM_WORLD);
#endif
      simplex_result res;
      simp = new simplex(min_params, max_params);
      simp->setProperties(ga_properties.simp_prop);
      res = simp->fit(initial_simplex, initial_simplex_res, func_fitness, args);
      delete simp;
      population[worse_index] = res.parameters;
      fitness[worse_index] = res.func_val;
      elite_individual = res.parameters;
      elite_fitness = res.func_val;
      elite_index = worse_index;
      safe_print("Refine completed in %6d iterations.\n", res.iteration);
      no_change_iteration = 0;
      worse_index = std::min_element(fitness.begin(), fitness.end()) - fitness.begin();
      print_population(fitness);
      fdiff = (fitness[elite_index] - fitness[worse_index]) * (fitness[elite_index] - fitness[worse_index]);
      if (fdiff < ga_properties.ftol) {
        safe_print("GA fit has converged!\n");
        print_population(fitness);

        result.elite_index = elite_index;
        result.fitness = fitness;
        result.population = population;
        result.eval_no = eval_no;
        result.iter_no = iter_no;
        return result;
      }
    } else
      print_population(fitness);

    if (ga_properties.crossover_scheme == ga_cross_blx_alpha)
      population = BLX_alpha_crossover();

    else if (ga_properties.crossover_scheme == ga_cross_normal_alpha)
      population = normal_alpha_crossover();

    else if (ga_properties.crossover_scheme == ga_cross_rayleigh_alpha)
      population = rayleigh_alpha_crossover();

    population = non_uniform_mutation();

#ifdef OPT_MPI_GA
    for (int i = 0; i < ga_properties.population_size; i++) {
      MPI_Bcast(population[i].data(), population[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
      MPI_Barrier(MPI_COMM_WORLD);
    }
#endif
    iter_no++;
    safe_print("\e[44m%.4f%% completed. Elite individual has not changed since %6d iterations.\e[0m\n",
               100. * (float) iter_no / (float) ga_properties.max_iteration, no_change_iteration);
  }
  result.elite_index = elite_index;
  result.fitness = fitness;
  result.population = population;
  result.eval_no = eval_no;
  result.iter_no = iter_no;
  return result;
}

void GA::calculate_fitness() {
  old_elite_index = elite_index;
  fitness.resize(ga_properties.population_size);
  for (int i = 0; i < ga_properties.population_size; i++) {
    if (i != elite_index) {
      std::vector<double> individual = population[i];
      double _fitness = (*func_fitness)(individual, args);
      eval_no++;
      if (std::isinf(_fitness)) _fitness = SMALL_DOUBLE;
      if (std::isnan(_fitness)) _fitness = SMALL_DOUBLE;
      fitness[i] = _fitness;
    }
  }

  elite_index = std::max_element(fitness.begin(), fitness.end()) - fitness.begin();
  worse_index = std::min_element(fitness.begin(), fitness.end()) - fitness.begin();

  elite_individual = population[elite_index];
  elite_fitness = fitness[elite_index];

  if (old_elite_index == elite_index) {
    no_change_iteration++;
  } else
    no_change_iteration = 0;
}

std::vector<std::vector<double>> GA::generate_random_population() {
  std::vector<std::vector<double>> new_population;
  for (int i = 0; i < ga_properties.population_size; i++) {
    std::vector<double> temp_individual(n_dim);
    for (int j = 0; j < n_dim; j++) {
      temp_individual[j] = gsl_ran_flat(r, min_params[j], max_params[j]);
    }
    new_population.push_back(temp_individual);
  }
  return new_population;
}

std::vector<std::vector<double>> GA::normal_alpha_crossover() {
  update_probability();
  std::vector<std::vector<double>> new_population;
  for (int i = 0; i < ga_properties.population_size; i++) {
    std::vector<double> new_individual(n_dim);
    if (i != elite_index) {
      std::vector<double> parent_1(n_dim);
      std::vector<double> parent_2(n_dim);
      int father_index, mother_index;
      if (ga_properties.selection_scheme == ga_sel_tournament) {
        select_parents_tournament(&father_index, &mother_index);
      } else {
        select_parents_probability(&father_index, &mother_index);
      }
      parent_1 = population[father_index];
      parent_2 = population[mother_index];
      for (int j = 0; j < n_dim; j++) {
        double min_X = std::min(parent_1[j], parent_2[j]);
        double max_X = std::max(parent_1[j], parent_2[j]);
        double dX = ga_properties.alpha * (max_X - min_X);
        min_X = std::max(min_X - dX, min_params[j]);
        max_X = std::min(max_X + dX, max_params[j]);

        do {
          double ran_num = gsl_ran_flat(r, 0, 1);
          double total_prob = selection_probability[father_index] + selection_probability[mother_index];
          if (ran_num < selection_probability[father_index] / total_prob) {
            new_individual[j] = parent_1[j] + gsl_ran_gaussian(r, dX);
          } else {
            new_individual[j] = parent_2[j] + gsl_ran_gaussian(r, dX);
          }
        } while ((new_individual[j] < min_params[j]) || (new_individual[j] > max_params[j]));
      }
    } else new_individual = elite_individual;
    new_population.push_back(new_individual);
  }
  return new_population;
}

std::vector<std::vector<double> > GA::rayleigh_alpha_crossover() {
  if (ga_properties.selection_scheme == ga_sel_probability) {
    update_probability();
  }

  std::vector<std::vector<double>> new_population;
  for (int i = 0; i < ga_properties.population_size; i++) {
    std::vector<double> new_individual(n_dim);
    if (i != elite_index) {
      std::vector<double> parent_1(n_dim);
      std::vector<double> parent_2(n_dim);
      int father_index, mother_index;
      if (ga_properties.selection_scheme == ga_sel_tournament) {
        select_parents_tournament(&father_index, &mother_index);
      } else {
        select_parents_probability(&father_index, &mother_index);
      }
      parent_1 = population[father_index];
      parent_2 = population[mother_index];
      for (int j = 0; j < n_dim; j++) {
        double min_X = std::min(parent_1[j], parent_2[j]);
        double max_X = std::max(parent_1[j], parent_2[j]);
        double dX = ga_properties.alpha * (max_X - min_X);
        min_X = std::max(min_X - dX, min_params[j]);
        max_X = std::min(max_X + dX, max_params[j]);

        do {
          //          double ran_num = gsl_ran_flat(r, 0, 1);
          //          if (ran_num < 0.5) {
          if (fitness[father_index] < fitness[mother_index]) {
            if (parent_1[j] > parent_2[j])
              new_individual[j] = parent_1[j] - gsl_ran_rayleigh(r, dX);
            else
              new_individual[j] = parent_1[j] + gsl_ran_rayleigh(r, dX);
          } else {
            if (parent_1[j] > parent_2[j])
              new_individual[j] = parent_2[j] + gsl_ran_rayleigh(r, dX);
            else
              new_individual[j] = parent_2[j] - gsl_ran_rayleigh(r, dX);
          }
        } while ((new_individual[j] < min_params[j]) || (new_individual[j] > max_params[j]));
      }
    } else new_individual = elite_individual;
    new_population.push_back(new_individual);
  }
  return new_population;
}

std::vector<std::vector<double>> GA::BLX_alpha_crossover() {
  if (ga_properties.selection_scheme == ga_sel_probability) {
    update_probability();
  }

  std::vector<std::vector<double>> new_population;
  for (int i = 0; i < ga_properties.population_size; i++) {
    std::vector<double> new_individual(n_dim);
    if (i != elite_index) {
      std::vector<double> parent_1(n_dim);
      std::vector<double> parent_2(n_dim);
      int father_index = 0, mother_index = 0;
      if (ga_properties.selection_scheme == ga_sel_tournament) {
        select_parents_tournament(&father_index, &mother_index);
      } else {
        select_parents_probability(&father_index, &mother_index);
      }
      parent_1 = population[father_index];
      parent_2 = population[mother_index];

      for (int j = 0; j < n_dim; j++) {
        double min_X = std::min(parent_1[j], parent_2[j]);
        double max_X = std::max(parent_1[j], parent_2[j]);
        double dX = ga_properties.alpha * (max_X - min_X);
        min_X = std::max(min_X - dX, min_params[j]);
        max_X = std::min(max_X + dX, max_params[j]);
        new_individual[j] = gsl_ran_flat(r, min_X, max_X);
      }
    } else new_individual = elite_individual;
    new_population.push_back(new_individual);
  }
  return new_population;
}

std::vector<std::vector<double>> GA::non_uniform_mutation() {
  std::vector<std::vector<double>> new_population;
  for (int i = 0; i < ga_properties.population_size; i++) {
    std::vector<double> new_individual(n_dim);
    if (i != elite_index) {
      new_individual = population[i];
      for (int j = 0; j < n_dim; j++) {
        double ran_num = gsl_ran_flat(r, 0, 1);
        if (ran_num < ga_properties.mutation_prob) {
          ran_num = gsl_ran_flat(r, 0, 1);
          if (ran_num < 0.5) {
            ran_num = gsl_ran_flat(r, 0, 1);
            new_individual[j] = new_individual[j] + (max_params[j] - new_individual[j])
                * (1 - pow(ran_num, pow(1 - (iter_no / ga_properties.max_iteration), ga_properties.mutation_conv)));
          } else {
            ran_num = gsl_ran_flat(r, 0, 1);
            new_individual[j] = new_individual[j] - (new_individual[j] - min_params[j])
                * (1 - pow(ran_num, pow(1 - (iter_no / ga_properties.max_iteration), ga_properties.mutation_conv)));
          }
        }
      }

    } else new_individual = elite_individual;
    new_population.push_back(new_individual);
  }
  return new_population;
}

void GA::update_probability() {
  double total_fitness = 0;
  double min_fitness = find_min(fitness);
  std::vector<double> modified_fitness = fitness;
  selection_probability.clear();
  cumulative_selection_probability.clear();

  for (int i = 0; i < ga_properties.population_size; i++) {
    if (min_fitness < 0) {
      modified_fitness[i] = fitness[i] + fabs(min_fitness);
    }
    total_fitness += modified_fitness[i];
  }

  for (int i = 0; i < ga_properties.population_size; i++) {
    double temp = modified_fitness[i] / total_fitness;
    if (std::isnan(temp) || std::isinf(temp)) temp = 0;
    selection_probability.push_back(temp);
  }
  for (int i = 0; i < ga_properties.population_size; i++) {
    cumulative_selection_probability.push_back(0);
    for (int j = 0; j <= i; j++) {
      cumulative_selection_probability[i] += selection_probability[j];
    }
  }
}

void GA::select_parents_probability(int *parent_1, int *parent_2) {
  int mother_index = 0;
  int father_index = 0;
  do {
    double ran_num = gsl_ran_flat(r, 0, 1);
    if (ran_num < cumulative_selection_probability[0]) {
      father_index = 0;
    } else {
      for (int j = 1; j < ga_properties.population_size; j++) {
        if (ran_num < cumulative_selection_probability[j] && ran_num > cumulative_selection_probability[j - 1]) {
          father_index = j;
          break;
        }
      }
    }

    ran_num = gsl_ran_flat(r, 0, 1);
    if (ran_num < cumulative_selection_probability[0]) {
      mother_index = 0;
    } else {
      for (int j = 1; j < ga_properties.population_size; j++) {
        if (ran_num < cumulative_selection_probability[j] && ran_num > cumulative_selection_probability[j - 1]) {
          mother_index = j;
          break;
        }
      }
    }
  } while (mother_index == father_index);
  *parent_1 = father_index;
  *parent_2 = mother_index;
}

void GA::select_parents_tournament(int *parent_1, int *parent_2) {
  int mother_index;
  int father_index;
  do {
    int candidate_1;
    int candidate_2;
    do {
      candidate_1 = gsl_ran_flat(r, 0, population.size());
      candidate_2 = gsl_ran_flat(r, 0, population.size());
    } while (candidate_1 == candidate_2);
    if (fitness[candidate_1] > fitness[candidate_2]) {
      mother_index = candidate_1;
    } else {
      mother_index = candidate_2;
    }

    do {
      candidate_1 = gsl_ran_flat(r, 0, population.size());
      candidate_2 = gsl_ran_flat(r, 0, population.size());
    } while (candidate_1 == candidate_2);
    if (fitness[candidate_1] > fitness[candidate_2]) {
      father_index = candidate_1;
    } else {
      father_index = candidate_2;
    }
  } while (father_index == mother_index);
  *parent_1 = father_index;
  *parent_2 = mother_index;
}

void GA::print_population(std::vector<double> _fitness) {
  safe_print("|%5d|", iter_no);
  for (int i = 0; i < n_dim; i++) {
    safe_print("%15s|", parameter_names[i].c_str());
  }
  safe_print("%15s|", "Fitness");
  safe_print("\n");
  for (int i = 0; i < ga_properties.population_size; i++) {
    if (i == elite_index) {
      safe_print("\e[42m");
    }
    safe_print("|%5d|", i);
    for (int j = 0; j < n_dim; j++) {
      double temp_var = population[i][j];
      if (temp_var >= 0)
        safe_print("%15e|", temp_var);
      else
        safe_print("%15e|", temp_var);
    }
    safe_print("%15e|", _fitness[i]);
    if (i == elite_index) {
      safe_print("\e[0m");
    }
    safe_print("\n");
  }
  safe_print("\n");
}

void GA::safe_print(const char *message, ...) {
  if (ga_properties.silent == 0) {

#ifdef OPT_MPI_GA
    if (world_rank == 0) {
#endif

    va_list _va_args;
    va_start(_va_args, message);
    vprintf(message, _va_args);
    va_end(_va_args);

#ifdef OPT_MPI_GA
    }
#endif

  }
}

double GA::find_min(std::vector<double> _fitness) {
  int index_res = 0;
  for (int i = 0; i < _fitness.size(); i++) {
    if (_fitness[i] < _fitness[index_res]) {
      index_res = i;
    }
  }
  return _fitness[index_res];
}

double GA::find_max(std::vector<double> _fitness) {
  int index_res = 0;
  for (int i = 0; i < _fitness.size(); i++) {
    if (_fitness[i] > _fitness[index_res]) {
      index_res = i;
    }
  }
  return _fitness[index_res];
}

const GA_properties &GA::getGaProperties() const {
  return ga_properties;
}

void GA::setGaProperties(const GA_properties &gaProperties) {
  ga_properties = gaProperties;
}

const std::vector<double> &GA::getFitness() const {
  return fitness;
}

const std::vector<std::vector<double>> &GA::getPopulation() const {
  return population;
}

const std::vector<std::string> &GA::getParameterNames() const {
  return parameter_names;
}

void GA::setParameterNames(const std::vector<std::string> &parameterNames) {
  parameter_names = parameterNames;
}
GA::~GA() {
  std::vector<std::vector<double> >().swap(population);
  std::vector<double>().swap(fitness);
  std::vector<double>().swap(selection_probability);
  std::vector<double>().swap(cumulative_selection_probability);
  gsl_rng_free(r);
}




