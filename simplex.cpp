//
// Created by Mehmet Deniz Aksulu on 2019-04-03.
//

#include "simplex.h"

#ifdef OPT_MPI_SIMPLEX
int world_rank, world_size;
#endif

simplex::simplex(std::vector<double> _min_param, std::vector<double> _max_param) {
#ifdef OPT_MPI_SIMPLEX
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
#endif
  // initialize gsl random number generator
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);
  gsl_rng_set(r, time(NULL));

  min_param = _min_param;
  max_param = _max_param;

  iteration = 0;

  n_dim = min_param.size();
  n_free_dim = min_param.size();
  for (int i = 0; i < min_param.size(); i++) {
    char temp_chr[255];
    sprintf(temp_chr, "Parameter %3d", i);
    std::string temp_str = temp_chr;
    parameter_names.push_back(temp_str);
    if (min_param[i] == max_param[i]) {
      n_free_dim--;
    } else free_parameter_index.push_back(i);
  }
  n_boundary_crossing = 0;
}

simplex_result simplex::fit(std::vector<double> initial_parameters,
                            double func_val,
                            double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                            void *_args) {
  func_fitness = _func_fitness;
  args = _args;
  simplex_vec = initialize_simplex(initial_parameters);

#ifdef OPT_MPI_SIMPLEX
  for (int i = 0; i < simplex_vec.size(); i++) {
    MPI_Bcast(simplex_vec[i].data(), simplex_vec[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
  }
#endif

  simplex_val.push_back(func_val);
  for (int i = 1; i < simplex_vec.size(); i++) {
    simplex_val.push_back(func_fitness(simplex_vec[i], args));
  }
  do {
    determine_indices();
    print_simplex();
    double x_tol = 0;
    double f_tol;
    for (int i = 0; i < simplex_vec[highest_index].size(); i++) {
      x_tol += pow(simplex_vec[highest_index][i] - simplex_vec[lowest_index][i], 2);
    }
    x_tol = sqrt(x_tol);
    f_tol = fabs(simplex_val[highest_index] - simplex_val[lowest_index]) / fabs(simplex_val[highest_index]);

    safe_print("\e[44m%.4f%% completed. xtol = %10e and ftol = %10e.\e[0m\n",
               100. * (float) iteration / (float) properties.max_iteration, x_tol, f_tol);
    if (f_tol < properties.f_tol || x_tol < properties.x_tol) {
      break;
    }
    calculate_centroid();
    safe_print("Reflecting.\n");
    std::vector<double> reflected = reflect();
    double reflected_val = func_fitness(reflected, args);
    if (reflected_val <= simplex_val[highest_index] && reflected_val > simplex_val[second_lowest_index]) {
      simplex_vec[lowest_index] = reflected;
      simplex_val[lowest_index] = reflected_val;
      iteration++;
      continue;
    } else if (reflected_val > simplex_val[highest_index]) {
      safe_print("Expanding.\n");
      std::vector<double> expanded = expand(reflected);
      double expanded_val = func_fitness(expanded, args);
      if (expanded_val > reflected_val) {
        simplex_vec[lowest_index] = expanded;
        simplex_val[lowest_index] = expanded_val;
        iteration++;
        continue;
      } else {
        simplex_vec[lowest_index] = reflected;
        simplex_val[lowest_index] = reflected_val;
        iteration++;
        continue;
      }
    } else if (reflected_val <= simplex_val[second_lowest_index]) {
      if (reflected_val > simplex_val[lowest_index]) {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(reflected);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > reflected_val) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      } else {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(simplex_vec[lowest_index]);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > simplex_val[lowest_index]) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      }
    }
    iteration++;
  } while (iteration < properties.max_iteration);

  determine_indices();
  simplex_result res;
  res.iteration = iteration;
  res.func_val = simplex_val[highest_index];
  res.parameters = simplex_vec[highest_index];
  return res;
}

simplex_result simplex::fit(std::vector<std::vector<double> > initial_simplex,
                            double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                            void *_args) {
  func_fitness = _func_fitness;
  args = _args;
  simplex_vec = initial_simplex;

#ifdef OPT_MPI_SIMPLEX
  for (int i = 0; i < simplex_vec.size(); i++) {
    MPI_Bcast(simplex_vec[i].data(), simplex_vec[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
  }
#endif

  for (int i = 0; i < simplex_vec.size(); i++) {
    simplex_val.push_back(func_fitness(simplex_vec[i], args));
  }
  do {
    determine_indices();
    print_simplex();
    double x_tol = 0;
    double f_tol;
    for (int i = 0; i < simplex_vec[highest_index].size(); i++) {
      x_tol += pow(simplex_vec[highest_index][i] - simplex_vec[lowest_index][i], 2);
    }
    x_tol = sqrt(x_tol);
    f_tol = fabs(simplex_val[highest_index] - simplex_val[lowest_index]) / fabs(simplex_val[highest_index]);

    safe_print("\e[44m%.4f%% completed. xtol = %10e and ftol = %10e.\e[0m\n",
               100. * (float) iteration / (float) properties.max_iteration, x_tol, f_tol);
    if (f_tol < properties.f_tol || x_tol < properties.x_tol) {
      break;
    }
    calculate_centroid();
    safe_print("Reflecting.\n");
    std::vector<double> reflected = reflect();
    double reflected_val = func_fitness(reflected, args);
    if (reflected_val <= simplex_val[highest_index] && reflected_val > simplex_val[second_lowest_index]) {
      simplex_vec[lowest_index] = reflected;
      simplex_val[lowest_index] = reflected_val;
      iteration++;
      continue;
    } else if (reflected_val > simplex_val[highest_index]) {
      safe_print("Expanding.\n");
      std::vector<double> expanded = expand(reflected);
      double expanded_val = func_fitness(expanded, args);
      if (expanded_val > reflected_val) {
        simplex_vec[lowest_index] = expanded;
        simplex_val[lowest_index] = expanded_val;
        iteration++;
        continue;
      } else {
        simplex_vec[lowest_index] = reflected;
        simplex_val[lowest_index] = reflected_val;
        iteration++;
        continue;
      }
    } else if (reflected_val <= simplex_val[second_lowest_index]) {
      if (reflected_val > simplex_val[lowest_index]) {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(reflected);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > reflected_val) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      } else {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(simplex_vec[lowest_index]);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > simplex_val[lowest_index]) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      }
    }
    iteration++;
  } while (iteration < properties.max_iteration);

  determine_indices();
  simplex_result res;
  res.iteration = iteration;
  res.func_val = simplex_val[highest_index];
  res.parameters = simplex_vec[highest_index];
  return res;
}

simplex_result simplex::fit(std::vector<std::vector<double> > initial_simplex, std::vector<double> initial_res,
                            double (*_func_fitness)(std::vector<double> _parameters, void *_args),
                            void *_args) {
  func_fitness = _func_fitness;
  args = _args;
  simplex_vec = initial_simplex;
  simplex_val = initial_res;

#ifdef OPT_MPI_SIMPLEX
  for (int i = 0; i < simplex_vec.size(); i++) {
    MPI_Bcast(simplex_vec[i].data(), simplex_vec[i].size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
  }
#endif

  do {
    determine_indices();
    print_simplex();
    double x_tol = 0;
    double f_tol;
    for (int i = 0; i < simplex_vec[highest_index].size(); i++) {
      x_tol += pow(simplex_vec[highest_index][i] - simplex_vec[lowest_index][i], 2);
    }
    x_tol = sqrt(x_tol);
    f_tol = fabs(simplex_val[highest_index] - simplex_val[lowest_index]) / fabs(simplex_val[highest_index]);

    safe_print("\e[44m%.4f%% completed. xtol = %10e and ftol = %10e.\e[0m\n",
               100. * (float) iteration / (float) properties.max_iteration, x_tol, f_tol);
    if (f_tol < properties.f_tol || x_tol < properties.x_tol) {
      break;
    }
    calculate_centroid();
    safe_print("Reflecting.\n");
    std::vector<double> reflected = reflect();
    double reflected_val = func_fitness(reflected, args);
    if (reflected_val <= simplex_val[highest_index] && reflected_val > simplex_val[second_lowest_index]) {
      simplex_vec[lowest_index] = reflected;
      simplex_val[lowest_index] = reflected_val;
      iteration++;
      continue;
    } else if (reflected_val > simplex_val[highest_index]) {
      safe_print("Expanding.\n");
      std::vector<double> expanded = expand(reflected);
      double expanded_val = func_fitness(expanded, args);
      if (expanded_val > reflected_val) {
        simplex_vec[lowest_index] = expanded;
        simplex_val[lowest_index] = expanded_val;
        iteration++;
        continue;
      } else {
        simplex_vec[lowest_index] = reflected;
        simplex_val[lowest_index] = reflected_val;
        iteration++;
        continue;
      }
    } else if (reflected_val <= simplex_val[second_lowest_index]) {
      if (reflected_val > simplex_val[lowest_index]) {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(reflected);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > reflected_val) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      } else {
        safe_print("Contracting.\n");
        std::vector<double> contracted = contract(simplex_vec[lowest_index]);
        double contracted_val = func_fitness(contracted, args);
        if (contracted_val > simplex_val[lowest_index]) {
          simplex_vec[lowest_index] = contracted;
          simplex_val[lowest_index] = contracted_val;
          iteration++;
          continue;
        } else {
          safe_print("Shrinking.\n");
          shrink();
          iteration++;
          continue;
        }
      }
    }
    iteration++;
  } while (iteration < properties.max_iteration);

  determine_indices();
  simplex_result res;
  res.iteration = iteration;
  res.func_val = simplex_val[highest_index];
  res.parameters = simplex_vec[highest_index];
  return res;
}

std::vector<std::vector<double> > simplex::initialize_simplex(std::vector<double> initial_parameters) {
  std::vector<std::vector<double> > _simplex_vec;
  _simplex_vec.push_back(initial_parameters);
  for (int i = 0; i < free_parameter_index.size(); i++) {
    std::vector<double> simplex_component = initial_parameters;
    double
        param_step = (max_param[free_parameter_index[i]] - min_param[free_parameter_index[i]]) * properties.step_size;
    double ran_num = gsl_ran_flat(r, 0, 1);
    if (ran_num < 0.5) {
      double param_temp = simplex_component[free_parameter_index[i]] + param_step;
      if (param_temp > max_param[free_parameter_index[i]])
        param_temp = max_param[free_parameter_index[i]];
      simplex_component[free_parameter_index[i]] = param_temp;
    } else {
      double param_temp = simplex_component[free_parameter_index[i]] - param_step;
      if (param_temp < min_param[free_parameter_index[i]])
        param_temp = min_param[free_parameter_index[i]];
      simplex_component[free_parameter_index[i]] = param_temp;
    }
    _simplex_vec.push_back(simplex_component);
  }
  return _simplex_vec;
}

void simplex::determine_indices() {
  highest_index = std::max_element(simplex_val.begin(), simplex_val.end()) - simplex_val.begin();
  lowest_index = std::min_element(simplex_val.begin(), simplex_val.end()) - simplex_val.begin();
  std::vector<double> temp_vector = simplex_val;
  temp_vector.erase(temp_vector.begin() + lowest_index);
  second_lowest_index = std::min_element(temp_vector.begin(), temp_vector.end()) - temp_vector.begin();
}

void simplex::calculate_centroid() {
  std::vector<double> temp_vector;
  for (int i = 0; i < simplex_vec[0].size(); i++) {
    double temp_param = 0;
    for (int j = 0; j < simplex_vec.size(); j++) {
      if (j != lowest_index) {
        temp_param += simplex_vec[j][i];
      }
    }
    temp_param = temp_param / (simplex_vec.size() - 1);
    temp_vector.push_back(temp_param);
  }
  centroid = temp_vector;
}

std::vector<double> simplex::reflect() {
  std::vector<double> temp_vector;
  for (int i = 0; i < centroid.size(); i++) {
    double temp_param = centroid[i] + properties.alpha * (centroid[i] - simplex_vec[lowest_index][i]);
    if (temp_param > max_param[i]) {
      n_boundary_crossing++;
      temp_param = max_param[i] - 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    if (temp_param < min_param[i]) {
      n_boundary_crossing++;
      temp_param = min_param[i] + 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    temp_vector.push_back(temp_param);
  }
  return temp_vector;
}

std::vector<double> simplex::expand(std::vector<double> reflected) {
  std::vector<double> temp_vector;
  for (int i = 0; i < centroid.size(); i++) {
    double temp_param = centroid[i] + properties.gamma * (reflected[i] - centroid[i]);
    if (temp_param > max_param[i]) {
      n_boundary_crossing++;
      temp_param = max_param[i] - 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    if (temp_param < min_param[i]) {
      n_boundary_crossing++;
      temp_param = min_param[i] + 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    temp_vector.push_back(temp_param);
  }
  return temp_vector;
}

std::vector<double> simplex::contract(std::vector<double> vec) {
  std::vector<double> temp_vector;
  for (int i = 0; i < centroid.size(); i++) {
    double temp_param = centroid[i] + properties.beta * (vec[i] - centroid[i]);

    if (temp_param > max_param[i]) {
      n_boundary_crossing++;
      temp_param = max_param[i] - 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    if (temp_param < min_param[i]) {
      n_boundary_crossing++;
      temp_param = min_param[i] + 1e-4 * (max_param[i] - min_param[i]) * fabs(sin(2.2 * n_boundary_crossing));
    }
    temp_vector.push_back(temp_param);
  }
  return temp_vector;
}

void simplex::shrink() {
  for (int i = 0; i < simplex_vec.size(); i++) {
    if (i != highest_index) {
      std::vector<double> temp_vector;
      for (int j = 0; j < centroid.size(); j++) {
        double temp_param =
            simplex_vec[highest_index][j] + properties.delta * (simplex_vec[i][j] - simplex_vec[highest_index][j]);
        if (temp_param > max_param[j]) {
          n_boundary_crossing++;
          temp_param = max_param[j] - 1e-4 * (max_param[j] - min_param[j]) * fabs(sin(2.2 * n_boundary_crossing));
        }
        if (temp_param < min_param[j]) {
          n_boundary_crossing++;
          temp_param = min_param[j] + 1e-4 * (max_param[j] - min_param[j]) * fabs(sin(2.2 * n_boundary_crossing));
        }
        temp_vector.push_back(temp_param);
      }
      simplex_vec[i] = temp_vector;
      simplex_val[i] = func_fitness(simplex_vec[i], args);
    }
  }
}

void simplex::print_simplex() {
  safe_print("|%5d|", iteration);
  for (int i = 0; i < simplex_vec[0].size(); i++) {
    safe_print("%15s|", parameter_names[i].c_str());
  }
  safe_print("%15s|", "Fitness");
  safe_print("\n");
  for (int i = 0; i < simplex_vec.size(); i++) {
    if (i == highest_index) {
      safe_print("\e[42m");
    } else if (i == lowest_index) {
      safe_print("\e[41m");
    }
    safe_print("|%5d|", i);
    for (int j = 0; j < simplex_vec[i].size(); j++) {
      double temp_var = simplex_vec[i][j];
      if (temp_var >= 0)
        safe_print("%15e|", temp_var);
      else
        safe_print("%15e|", temp_var);
    }
    safe_print("%15e|", simplex_val[i]);
    if (i == highest_index || i == lowest_index) {
      safe_print("\e[0m");
    }
    safe_print("\n");
  }
  safe_print("\n");
}

void simplex::safe_print(const char *message, ...) {
  if (properties.silent == 0) {

#ifdef OPT_MPI_SIMPLEX
    if (world_rank == 0) {
#endif

    va_list _va_args;
    va_start(_va_args, message);
    vprintf(message, _va_args);
    va_end(_va_args);

#ifdef OPT_MPI_SIMPLEX
    }
#endif

  }
}

const simplex_properties &simplex::getProperties() const {
  return properties;
}

void simplex::setProperties(const simplex_properties &properties) {
  simplex::properties = properties;
}
simplex::~simplex() {
  std::vector<std::vector<double> >().swap(simplex_vec);
  std::vector<double>().swap(min_param);
  std::vector<double>().swap(max_param);
  std::vector<int>().swap(free_parameter_index);
  std::vector<double>().swap(simplex_val);
  std::vector<double>().swap(centroid);
  gsl_rng_free(r);
}




